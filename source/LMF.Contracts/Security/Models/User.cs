using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMF.Contracts.Security.Models
{
    public class User
    {
        [Key]
        public long Id { get; set; }
        
        [EmailAddress]
        [Required]
        [MaxLength(100)] 
        [Index(IsUnique = true)]  
        public string EmailAddress { get; set; }

        public bool Admin { get; set; }
        public bool ScanStops { get; set; }
        public bool SelfRegister { get; set; }
        public bool ApproveRegistrations { get; set; }
        public bool ViewReports { get; set; }
    }
}