using LMF.Contracts.Security.Models;

namespace LMF.Contracts.Security
{
    public interface IUserPasswordRepository
    {
        UserPassword Find(long userId);
    }
}