﻿using System.Web.Mvc;

namespace LMF.Web.Controllers
{
    [Authorize]
    public class ClientAppController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}