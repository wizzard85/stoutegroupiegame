﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using LMF.Contracts.Security;
using LMF.Contracts.Security.Models;
using LMF.DataAccess.Security;
using LMF.Security;
using LMF.Web.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace LMF.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        private readonly IUserRepository _userRepository ;
        private IUserRoleRepository _userRoleRepository ;
        private readonly IUserPasswordRepository _userPasswordRepository ;

        public AuthenticationController()
        {
            _userRepository = new UserRepository();
            _userRoleRepository = new UserRoleRepository();
            _userPasswordRepository = new UserPasswordRepository();
        }

        //
        // GET: /Authentication/
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "ClientApp");
            }
            return View();
        }

        //
        // POST: /Authentication/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = GetUser(model.UserName, model.Password);
                if (user != null || AreThereNoUsers())
                {
                    SignIn(user, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
                
                ModelState.AddModelError("", "Invalid username or password.");
            }

            return View(model);
        }

        //
        // POST: /Authentication/LogOff
        //[HttpPost]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Authentication");
        }

        private User GetUser(string userName, string password)
        {
            var user = _userRepository.Find(userName);
            if (user == null) return null;

            var userPassword = _userPasswordRepository.Find(user.Id);
            if (userPassword == null) return null;

            if (!PasswordHash.ValidatePassword(password, userPassword.HashedPassword)) return null;

            return user;
        }

        private bool AreThereNoUsers()
        {
            return _userRepository.CountUsers() == 0;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            
            return RedirectToAction("Index", "ClientApp");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void SignIn(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            var claims = new List<Claim>();

            if (user != null)
            {
                var userRoles = _userRoleRepository.Find(user.EmailAddress);
                claims.Add(new Claim(ClaimTypes.Email, user.EmailAddress));
                claims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole.Name)));
            }
            else 
            {
                claims.Add(new Claim(ClaimTypes.Email, "NoUsersAdmin"));
            }

            var id = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, id);
        }
	}
}