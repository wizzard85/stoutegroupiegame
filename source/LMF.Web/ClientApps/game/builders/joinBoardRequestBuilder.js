﻿function JoinBoardRequestBuilder() {
}

JoinBoardRequestBuilder.prototype.withCharacterName = function(name) {
    var self = this;
    self.characterName = name;
    return self;
}

JoinBoardRequestBuilder.prototype.build = function () {
    var self = this;
    return {
        characterName: self.characterName
    }
}