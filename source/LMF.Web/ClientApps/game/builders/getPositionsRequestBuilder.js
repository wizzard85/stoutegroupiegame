﻿function GetPositionsRequestBuilder() {

}

GetPositionsRequestBuilder.prototype.withSectorKey = function (sectorKey) {
    var self = this;
    self.sectorKey = sectorKey;
    return self;
}

GetPositionsRequestBuilder.prototype.build = function () {
    var self = this;

    return {
        sectorKey: self.sectorKey
    }
}