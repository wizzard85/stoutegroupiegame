﻿function CallbackInfoBuilder() {

}

CallbackInfoBuilder.prototype.withCallback = function (callback) {
    var self = this;
    self.callback = callback;
    return self;
}

CallbackInfoBuilder.prototype.withThis = function (thisForCallback) {
    var self = this;
    self.thisForCallback = thisForCallback;
    return self;
}

CallbackInfoBuilder.prototype.build = function () {
    var self = this;

    return {
        callback: self.callback,
        thisForCallback: self.thisForCallback || self
    }
}