﻿function World(elementName, network) {
    var self = this;
    self.elementName = elementName;
    self.network = network;
    self.tileSpritesInView = {};
    self.sectorsViewed = {};
    self.network.addOnBattleFinishedCallback(self.finishBattle, self);
    self.network.addOnBattleStoppedCallback(self.stopBattle, self);
}

World.prototype.connect = function () {
    var self = this;
    return self.network.connect();
}

World.prototype.joinBoard = function(characterName) {
    var self = this;
    var joinBoardRequest = new JoinBoardRequestBuilder()
        .withCharacterName(characterName)
        .build();
    return self.network.joinBoard(joinBoardRequest).then(function (joinBoardResponse) {
        if (joinBoardResponse.hasErrors) {
            return $.Deferred().reject(joinBoardResponse.errors);
        }

        self.board = new Board(joinBoardResponse.boardDefinition.sizeX, joinBoardResponse.boardDefinition.sizeY,
            joinBoardResponse.boardDefinition.sectorSizeX, joinBoardResponse.boardDefinition.sectorSizeY);
    });
}

World.prototype.startBattle = function(positionKey) {
    var self = this;
    var battleRequest = new BattleRequestBuilder()
        .withPositionKey(positionKey)
        .build();

    self.network.startBattle(battleRequest);
}

World.prototype.finishBattle = function (battleResponse) {
    var self = this;
    var position = self.board.takePosition(battleResponse);
    if (position) {
        self.__updateTileSpriteStatus(position);
    }
}

World.prototype.stopBattle = function (battleResponse) {
    //todo show errors to the user
    console.log(battleResponse);
}

World.prototype.__getTileSpriteTint = function (boardPosition) {
    return boardPosition.taken ? 0x611DA6 : 0xF6E91B;
}

World.prototype.__updateTileSpriteStatus = function (boardPosition) {
    var self = this;
    var tileSprite = self.tileSpritesInView[boardPosition.key];
    if (tileSprite) {
        tileSprite.tint = self.__getTileSpriteTint(boardPosition);
    }
}

World.prototype.render = function () {
    var self = this;
    var previousPointerPosition;
    var dragStartingPoint;

    var gameContentElement = $('#' + self.elementName);
    var gameWidth = gameContentElement.width();
    var gameHeight = gameContentElement.height();

    var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, self.elementName, { preload: onPreload, create: onCreate, update: onUpdate });

    $(window).resize(resizeGame);

    function onPreload() {
        game.load.image(self.board.tileImage.name, self.board.tileImage.url);
    }

    function onCreate() {
        game.world.setBounds(0, 0, self.board.width, self.board.height);
        game.stage.backgroundColor = self.board.backgroundColour;
        game.input.addMoveCallback(onInputMove, this);
    }

    //todo consider adding a check to this function to only run content if camera has changed
    function onUpdate() {
        var boardPositionsInView = getBoardPositionsInView();
        removeTileSpritesNotInView(boardPositionsInView);
        addTileSpritesForPositionsNewInView(boardPositionsInView);

        trackSectorsViewed(boardPositionsInView);
        loadPositionStatusesForPositionsInViewdSectors();
    }

    function getBoardPositionsInView() {
        var boardPositionsInView = {};
        for (var positionKey in self.board.positions) {
            var boardPosition = self.board.positions[positionKey];
            var isPositionOnCamera = wouldPositionBeOnCamera(boardPosition);

            if (isPositionOnCamera) {
                boardPositionsInView[boardPosition.key] = boardPosition;
            }
        }

        return boardPositionsInView;
    }

    function removeTileSpritesNotInView(boardPositionsInView) {
        function isPositionInView(positionKey) {
            return boardPositionsInView[positionKey];
        }

        function removeSpriteFromView(positionKey) {
            self.tileSpritesInView[positionKey].destroy();
            delete self.tileSpritesInView[positionKey];
        }

        for (var positionKey in self.tileSpritesInView) {
            if (!isPositionInView(positionKey)) {
                removeSpriteFromView(positionKey);
            }
        }
    }

    function addTileSpritesForPositionsNewInView(boardPositionsInView) {
        function isPositionNewInView(boardPosition) {
            return !self.tileSpritesInView[boardPosition.key];
        }

        function addSpriteToView(boardPosition) {
            var tileSprite = createTileSprite(boardPosition);
            self.tileSpritesInView[boardPosition.key] = tileSprite;
        }

        for (var positionKey in boardPositionsInView) {
            var boardPosition = boardPositionsInView[positionKey];

            if (isPositionNewInView(boardPosition)) {
                addSpriteToView(boardPosition);
            }
        }
    }

    function trackSectorsViewed(boardPositionsInView) {
        function recordSectorAsViewed(sectorKey) {
            if (self.sectorsViewed[sectorKey] === undefined) {
                self.sectorsViewed[sectorKey] = false;
            }
        }

        for (var positionKey in boardPositionsInView) {
            var boardPosition = self.board.positions[positionKey];
            recordSectorAsViewed(boardPosition.sector);
        }
    }

    //todo split out the functionality that gets a list of sectors that should have the statues for their positions loaded
    function loadPositionStatusesForPositionsInViewdSectors() {
        function havePositionsForSectorBeenLoaded(sectorKey) {
            return self.sectorsViewed[sectorKey] === true;
        }

        function setSectorPositionsAsLoaded(sectorKey) {
            self.sectorsViewed[sectorKey] = true;
        }

        function checkForResponseErrors(getPositionsResponse) {
            if (getPositionsResponse.hasErrors) {
                return $.Deferred().reject(getPositionsResponse.errors);
            }
            return getPositionsResponse;
        }

        function updateBoardPositions(getPositionsResponse) {
            getPositionsResponse.positions.forEach(function (position) {
                //todo move the taking of the position to the board
                var boardPosition = self.board.positions[position.key];
                boardPosition.taken = position.owner ? true : false;

                self.__updateTileSpriteStatus(boardPosition);
            });
            return getPositionsResponse;
        }

        for (var sectorKey in self.sectorsViewed) {
            //todo this is a simple implementation of not loading sector positions twice, it assumes no error and need for retry, it should be updated to only set loaded when they are successfully loaded, but then it shouldn't double query so the loading status will need to be considered
            if (havePositionsForSectorBeenLoaded(sectorKey)) continue;
            setSectorPositionsAsLoaded(sectorKey);

            var getPositionsRequest = new GetPositionsRequestBuilder()
                            .withSectorKey(sectorKey)
                            .build();

            self.network.getPositions(getPositionsRequest)
                .then(checkForResponseErrors)
                .then(updateBoardPositions);
        }
    }

    function wouldPositionBeOnCamera(boardPosition) {
        return game.camera.view.contains(boardPosition.tileX, boardPosition.tileY) ||
            game.camera.view.contains(boardPosition.tileX + boardPosition.tileWidth, boardPosition.tileY + boardPosition.tileHeight) ||
            game.camera.view.contains(boardPosition.tileX + boardPosition.tileWidth, boardPosition.tileY) ||
            game.camera.view.contains(boardPosition.tileX, boardPosition.tileY + boardPosition.tileHeight);
    }

    function createTileSprite(boardPosition) {
        var tileSprite = game.add.sprite(boardPosition.tileX, boardPosition.tileY, boardPosition.tileImage);
        tileSprite.positionKey = boardPosition.key;
        tileSprite.inputEnabled = true;
        tileSprite.input.pixelPerfectClick = true;
        tileSprite.tint = self.__getTileSpriteTint(boardPosition);

        tileSprite.events.onInputUp.add(onTileInputUp, this);

        return tileSprite;
    }

    function onTileInputUp(tileSprite, pointer) {
        if (dragStartingPoint && (dragStartingPoint.x !== pointer.position.x || dragStartingPoint.y !== pointer.position.y)) return;
        self.startBattle(tileSprite.positionKey);
    }

    function onInputMove() {
        moveCameraByPointer(game.input.mousePointer);
        moveCameraByPointer(game.input.pointer1);
    }

    function moveCameraByPointer(pointer) {
        if (!pointer.timeDown) {
            return;
        }
        if (pointer.isDown) {
            if (previousPointerPosition) {
                game.camera.x += previousPointerPosition.x - pointer.position.x;
                game.camera.y += previousPointerPosition.y - pointer.position.y;
            } else {
                dragStartingPoint = pointer.position.clone();
            }
            previousPointerPosition = pointer.position.clone();
        }
        if (pointer.isUp) {
            previousPointerPosition = null;
            dragStartingPoint = null;
        }
    }

    function resizeGame() {
        var newGameWidth = gameContentElement.width();
        var newGameHeight = gameContentElement.height();

        game.width = newGameWidth;
        game.height = newGameHeight;

        if (game.renderType === Phaser.WEBGL) {
            game.renderer.resize(newGameWidth, newGameHeight);
        }
    }
}