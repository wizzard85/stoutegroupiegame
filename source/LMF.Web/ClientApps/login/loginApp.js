﻿(function () {
    'use strict';
    
    var app = angular.module('loginApp', [
        // Angular modules 
        'ngAnimate',        
        'ngMaterial',
        'ngMessages'
    ]);

    app.directive('loginErrors', ['loginErrors', function (loginErrors) {
        function link(scope, element, attrs) {
            scope.errors = loginErrors;
        }
        
        var template = '<div data-ng-messages="errors">';
        template += '   <div data-ng-message-exp="propertyName" class="errorMessage">{{errors[propertyName]}}</div>';
        template += '</div>';

        return {
            link: link,
            restrict: 'E',
            scope: {
                propertyName: '@'
            },
            template: template,
            replace: true
        };
    }]);
    
    // Handle routing errors and success events
    app.run([function () {
        }]);        
}());