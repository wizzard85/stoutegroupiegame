﻿(function () {
    'use strict';

    var app = angular.module('app');
   
    // Configure the routes and route resolvers
    function routeConfigurator($routeProvider, routes) {

        routes.forEach(function (r) {
            $routeProvider.when(r.url, r.config);
        });
        $routeProvider.otherwise({ redirectTo: '/' });
    }
    app.config(['$routeProvider', 'routes', routeConfigurator]);

    // Define the routes 
    function getRoutes() {
        return [
            {
                url: '/',
                config: {
                    templateUrl: '/ClientApps/main/sections/dashboard/dashboard.html',
                    title: 'dashboard',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-dashboard"></i> Dashboard'
                    }
                }
            }, {
                url: '/game',
                config: {
                    title: 'game',
                    templateUrl: '/ClientApps/main/sections/game/game.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-gamepad"></i> Game',
                        //todo try use enums here instead
                        roles: ['Admin']
                    }
                }
            }, {
                url: '/users',
                config: {
                    title: 'users',
                    templateUrl: '/ClientApps/main/sections/users/users.html',
                    settings: {
                        nav: 2,
                        content: '<i class="fa fa-user"></i> Users',
                        //todo try use enums here instead
                        roles: ['Admin']
                    }
                }
            }
        ];
    }

    // Collect the routes
    app.constant('routes', getRoutes());
}());