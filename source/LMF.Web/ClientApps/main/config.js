﻿(function () {
    'use strict';

    var app = angular.module('app');
    var toastr = window.toastr;

    // Configure Toastr
    toastr.options.timeOut = 4000;
    toastr.options.positionClass = 'toast-bottom-right';

    var modelsServiceName = 'breeze/Models';

    var events = {
        controllerActivateSuccess: 'controller.activateSuccess',
        spinnerToggle: 'spinner.toggle'
    };

    var config = {
        appErrorPrefix: '[Error] ', //Configure the exceptionHandler decorator
        docTitle: 'LMF: ',
        events: events,
        modelsServiceName: modelsServiceName,
        version: '1.0.0',
        dateFormat: 'DD MMM YYYY',
        dateTimeFormat: 'HH:mm:ss DD MMM YYYY'
    };

    //Add 3rd party libraries as values so that they can be injected 
    //through the Angular DI system
    app.value('config', config);
    app.value('toastr', toastr);
    app.value('$', window.$);
    app.value('_', window._);
    app.value('moment', window.moment);
    app.value('localforage', window.localforage);
    app.value('Board', Board);
    app.value('World', World);
    
    app.config(['$logProvider', function ($logProvider) {
        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
    }]);

    app.config(['$animateProvider', function ($animateProvider) {
        $animateProvider.classNameFilter(/^((?!(fa-spin)).)*$/);
    }]);
    
    //#region Configure the common services via commonConfig
    app.config(['commonConfigProvider', function (cfg) {
        cfg.config.controllerActivateSuccessEvent = config.events.controllerActivateSuccess;
        cfg.config.spinnerToggleEvent = config.events.spinnerToggle;
    }]);
    //#endregion
}());