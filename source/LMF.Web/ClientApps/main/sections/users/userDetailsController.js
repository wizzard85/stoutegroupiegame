﻿(function() {
    'use strict';
    var controllerId = 'userDetails';

    function detailsDialog(common, $scope, $mdDialog, options, dataService, _) {
        var vm = this;

        function destroyLocalCopyOfUserPassword() {
            var userPassword = $scope.userPassword;
            if (!userPassword) {
                return;
            }

            userPassword.entityAspect.setDetached();
            userPassword.HashedPassword = '';
            delete $scope.userPassword;
        }

        function cleanUpSecurityStuff() {
            destroyLocalCopyOfUserPassword();
            return common.$q.when();
        }

        function cancelChanges() {
            options.user.entityAspect.rejectChanges();
        }

        function save() {
            var userPassword = $scope.userPassword;

            function saveSucceeded() {
                $mdDialog.hide(options.user);
            }

            function saveUserPassword() {
                if (!userPassword) {
                    return common.$q.when();
                }

                userPassword.UserId = options.user.Id;
                dataService.attachEntity(userPassword, options.isNew);
                return dataService.saveChanges();
            }

            function isPasswordInvalid() {
                return userPassword && !userPassword.entityAspect.validateProperty('HashedPassword');
            }

            if (isPasswordInvalid()) {
                return;
            }

            dataService.saveChanges()
                .then(saveUserPassword)
                .then(cleanUpSecurityStuff)
                .then(saveSucceeded);
        }

        function cancel() {
            cleanUpSecurityStuff();
            cancelChanges();
            $mdDialog.cancel();
        }

        function toggleChangeUserPassword() {
            $scope.changePassword = !$scope.changePassword;

            if ($scope.changePassword) {
                $scope.userPassword = dataService.createDetachedEntity("UserPassword", {});
            } else {
                delete $scope.userPassword;
            }
        }

        function activate() {
            common.activateController([], controllerId)
                .then(function() {
                    $scope.user = options.user;
                    $scope.isNew = options.isNew;

                    if (options.isNew) {
                        toggleChangeUserPassword();
                    }
                });
        }

        vm.save = save;
        vm.cancel = cancel;
        vm.toggleChangeUserPassword = toggleChangeUserPassword;

        activate();
    }

    angular.module('app').controller(controllerId, ['common', '$scope', '$mdDialog', 'options', 'dataService', '_', detailsDialog]);
}());