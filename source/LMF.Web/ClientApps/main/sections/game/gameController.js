﻿(function() {
    'use strict';
    var controllerId = 'game';

    function game($scope, common, $mdSidenav, $timeout, Board, World, $) {
        var vm = this;

        function toggleList() {
            //todo implement this so that you don't have to do it on every view and so that it closes when you select something from the menu
            $mdSidenav('left').toggle();
        }

        function onConnectionStateChanged(state) {
            $scope.isConnected = state.newState === $.signalR.connectionState.connected;
            $scope.isConnecting = state.newState === $.signalR.connectionState.connecting;
            if (state.newState === $.signalR.connectionState.disconnected) {
                $scope.isInGame = false;    
            }
            
            $timeout(function() {
                $scope.$digest();
            });
        }

        function connect() {
            vm.world.connect();
        }

        function join() {
            $scope.isJoining = true;
            vm.world.joinBoard(vm.characterName)
                .then(function() {
                    $scope.isJoining = false;
                    $scope.isInGame = true;
                    $timeout(function() {
                        return vm.world.render();
                    }, 100);
                }, function(errors) {
                    $scope.isJoining = false;
                    alert(errors);
                });
        }

        function activate() {
            common.activateController([], controllerId)
                .then(function () {
                    //todo move this to some location that will persist while the user is logged in so that it doesn't loose the client side state
                    vm.connection = $.connection;
                    //todo clear previous subscriptions
                    vm.connection.hub.stateChanged(onConnectionStateChanged);
                    vm.networkClient = new NetworkClient(vm.connection);
                    vm.world = new World('gameContent', vm.networkClient);
                });
        }

        vm.toggleList = toggleList;
        vm.connect = connect;
        vm.join = join;

        activate();
    }

    angular.module('app').controller(controllerId, ['$scope', 'common', '$mdSidenav', '$timeout', 'Board', 'World', '$', game]);
}());