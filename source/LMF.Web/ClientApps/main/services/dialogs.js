﻿(function () {
    'use strict';

    var serviceId = 'dialogs';

    function dialogs($mdDialog) {
        function deletePrompt(message, event) {
            var confirm = $mdDialog.confirm()
                  .title('Delete Confirmation')
                  .content(message)
                  .ariaLabel(message)
                  .parent(angular.element(document.body))
                  .targetEvent(event)
                  .ok('Yes')
                  .cancel('No');
            return $mdDialog.show(confirm);
        }

        var service = {
            deletePrompt: deletePrompt,
        };

        return service;
    }
    angular.module('app').factory(serviceId, ['$mdDialog', dialogs]);
}());