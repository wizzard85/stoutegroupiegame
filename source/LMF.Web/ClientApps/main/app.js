﻿(function () {
    'use strict';
    
    var app = angular.module('app', [
        // Angular modules 
        'ngAnimate',        
        'ngRoute',          
        'ngSanitize',       
        'ngMaterial',
        'ngMessages',

        // Custom modules 
        'common',           

        // 3rd Party Modules
        'breeze.angular', 
        'breeze.directives'
    ]);
    
    // Handle routing errors and success events
    app.run(['$route', 'breeze',  function ($route, breeze) {
            // Include $route to kick start the router.
        }]);        
}());