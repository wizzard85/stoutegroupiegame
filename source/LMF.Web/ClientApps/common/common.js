(function () {
    'use strict';

    // Define the common module 
    // Contains services:
    //  - common
    //  - logger
    var commonModule = angular.module('common', []);

    // Must configure the common service and set its 
    // events via the commonConfigProvider
    commonModule.provider('commonConfig', function () {
        this.config = {
            // These are the properties we need to set
            //controllerActivateSuccessEvent: '',
            //spinnerToggleEvent: ''
        };

        this.$get = function () {
            return {
                config: this.config
            };
        };
    });

    function common($q, $rootScope, commonConfig, logger) {
        var throttles = {};

        function $broadcast() {
            return $rootScope.$broadcast.apply($rootScope, arguments);
        }

        function activateController(promises, controllerId) {
            return $q.all(promises).then(function (eventArgs) {
                var data = { controllerId: controllerId };
                $broadcast(commonConfig.config.controllerActivateSuccessEvent, data);
            });
        }

        var service = {
            // common angular dependencies
            $broadcast: $broadcast,
            $q: $q,
            // generic
            activateController: activateController,
            logger: logger
        };

        return service;
    }

    commonModule.factory('common', ['$q', '$rootScope', 'commonConfig', 'logger', common]);
}());