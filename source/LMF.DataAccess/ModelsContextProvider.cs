﻿using Breeze.ContextProvider;
using Breeze.ContextProvider.EF6;
using LMF.Contracts.Security.Models;
using LMF.Security;

namespace LMF.DataAccess
{
    public class ModelsContextProvider : EFContextProvider<ModelsContext>
    {
        protected override bool BeforeSaveEntity(EntityInfo entityInfo)
        {
            var entity = entityInfo.Entity as UserPassword;
            if (entity != null)
            {
                entity.HashedPassword = PasswordHash.CreateHash(entity.HashedPassword);
            }

            return base.BeforeSaveEntity(entityInfo);
        }
    }
}