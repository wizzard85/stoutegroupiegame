using System.Data.SqlClient;
using System.Linq;
using LMF.Contracts.Security;
using LMF.Contracts.Security.Models;

namespace LMF.DataAccess.Security
{
    public class UserPasswordRepository : IUserPasswordRepository
    {
        private readonly ModelsContext _context;

        public UserPasswordRepository()
        {
            _context = new ModelsContext();
        }

        public UserPasswordRepository(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            _context = new ModelsContext(connection, true);
        }

        public UserPassword Find(long userId)
        {
            return _context.UserPasswords.FirstOrDefault(userPassword => userPassword.UserId == userId);
        }
    }
}