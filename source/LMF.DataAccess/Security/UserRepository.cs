using System.Data.SqlClient;
using System.Linq;
using LMF.Contracts.Security;
using LMF.Contracts.Security.Models;

namespace LMF.DataAccess.Security
{
    public class UserRepository : IUserRepository
    {
        private readonly ModelsContext _context;

        public UserRepository()
        {
            _context = new ModelsContext();
        }

        public UserRepository(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            _context = new ModelsContext(connection, true);
        }

        public User Find(string emailAddress)
        {
            return _context.Users.FirstOrDefault(user => user.EmailAddress == emailAddress);
        }

        public long CountUsers()
        {
            return _context.Users.Count();
        }
    }
}