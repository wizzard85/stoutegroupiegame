using System.Collections.Generic;
using System.Linq;
using LMF.Contracts.Security;
using LMF.Contracts.Security.Models;

namespace LMF.DataAccess.Security
{
    public class UserRoleRepository : IUserRoleRepository
    {
        private readonly ModelsContext _context;

        public UserRoleRepository()
        {
            _context = new ModelsContext();
        }

        public IEnumerable<UserRole> Find(string emailAddress)
        {
            var userRoles = new List<UserRole>();

            if (emailAddress == "NoUsersAdmin")
            {
                userRoles.Add(new UserRole { Name = "Admin" });
                return userRoles;
            }

            var user = _context.Users.FirstOrDefault(u => u.EmailAddress == emailAddress);
            if (user == null) return userRoles;

            if (user.Admin) userRoles.Add(new UserRole{ Name = "Admin"});
            if (user.ApproveRegistrations) userRoles.Add(new UserRole { Name = "Approve Registrations" });
            if (user.ScanStops) userRoles.Add(new UserRole { Name = "Scan Stops" });
            if (user.SelfRegister) userRoles.Add(new UserRole { Name = "Self Register" });
            if (user.ViewReports) userRoles.Add(new UserRole { Name = "View Reports" });

            return userRoles;
        }
    }
}