﻿using System.Collections.Generic;
using LMF.Game.Models.Builders;

namespace LMF.Game.Tests
{
   public class BoardTestDataBuilder
    {
        private readonly List<string> _characters = new List<string>();
       private IBoardDefinition _boardDefinition;

       public BoardTestDataBuilder WithCharacters(params string[] characterNames)
        {
            _characters.AddRange(characterNames);
            return this;
        }

       public BoardTestDataBuilder WithDefinition(IBoardDefinition boardDefinition)
       {
           _boardDefinition = boardDefinition;
           return this;
       }

       public Board Build()
        {
            _boardDefinition = _boardDefinition ?? new UniformHexagonBoardDefinitionBuilder()
                    .WithSize(100, 100)
                    .Build();

            var board = new Board(_boardDefinition);

            foreach (var characterName in _characters)
            {
                board.AddCharacter(characterName);
            }

            return board;
        }
    }

}
