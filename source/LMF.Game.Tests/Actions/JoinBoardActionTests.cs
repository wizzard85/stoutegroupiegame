﻿using LMF.Game.Actions;
using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;
using NUnit.Framework;

namespace LMF.Game.Tests.Actions
{
    [TestFixture]
    public class JoinBoardActionTests
    {
        [Test]
        public void Join_GivenCharacterIsNotOnBoard_ShouldReturnResponseWithUniformHexagonBoardThatIs100x100()
        {
            //Arrange
            var expectedBoardSizeX = 100;
            var expectedBoardSizeY = 100;
            var characterName = "Mark";
            var joinBoardRequest = new JoinBoardRequestBuilder()
                .WithCharacterName(characterName)
                .Build();
            var board = (IBoard) new BoardTestDataBuilder()
                .Build();
            var joinBoardAction = CreateJoinBoardAction(board);
            //Act
            var actualJoinBoardResponse = joinBoardAction.Join(joinBoardRequest);
            //Assert
            Assert.NotNull(actualJoinBoardResponse);
            Assert.NotNull(actualJoinBoardResponse.boardDefinition);
            var boardDefinition = actualJoinBoardResponse.boardDefinition as UniformHexagonBoardDefinition;
            Assert.NotNull(boardDefinition);
            Assert.AreEqual(expectedBoardSizeX, boardDefinition.sizeX);
            Assert.AreEqual(expectedBoardSizeY, boardDefinition.sizeY);
        }

        [Test]
        public void Join_GivenCharacterIsNotOnBoard_ShouldAddTheCharacterToBoard()
        {
            //Arrange
            var characterName = "Brendon";
            var expectedCharacters = new [] {characterName};
            var joinBoardRequest = new JoinBoardRequestBuilder()
                .WithCharacterName(characterName)
                .Build();
            var board = (IBoard) new BoardTestDataBuilder()
                .Build();
            var joinBoardAction = CreateJoinBoardAction(board);
            //Act
            joinBoardAction.Join(joinBoardRequest);
            //Assert
            CollectionAssert.AreEquivalent(expectedCharacters, board.Characters);
        }

        [Test]
        public void Join_GivenCharacterIsAlreadyOnBoard_ShouldNotAddTheCharacterToTheBoardAgain()
        {
            //Arrange
            var characterName = "Brendon";
            var expectedCharacters = new [] {characterName};
            var joinBoardRequest = new JoinBoardRequestBuilder()
                .WithCharacterName(characterName)
                .Build();
            var board = (IBoard) new BoardTestDataBuilder()
                .Build();
            var joinBoard1Action = CreateJoinBoardAction(board);
            var joinBoard2Action = CreateJoinBoardAction(board);
            joinBoard1Action.Join(joinBoardRequest);
            //Act
            joinBoard2Action.Join(joinBoardRequest);
            //Assert
            CollectionAssert.AreEquivalent(expectedCharacters, board.Characters);
        }

        [Test]
        public void Join_GivenCharacterIsAlreadyOnBoard_ShouldReturnResponseWithError()
        {
            //Arrange
            var characterName = "Tubbeh";
            var joinBoardRequest = new JoinBoardRequestBuilder()
                .WithCharacterName(characterName)
                .Build();
            var board = (IBoard) new BoardTestDataBuilder()
                .Build();
            var joinBoard1Action = CreateJoinBoardAction(board);
            var joinBoard2Action = CreateJoinBoardAction(board);
            joinBoard1Action.Join(joinBoardRequest);
            //Act
            var actualJoinBoardResponse = joinBoard2Action.Join(joinBoardRequest);
            //Assert
            Assert.NotNull(actualJoinBoardResponse);
            Assert.Null(actualJoinBoardResponse.boardDefinition);
            Assert.NotNull(actualJoinBoardResponse.errors);
            Assert.AreEqual(1, actualJoinBoardResponse.errors.Count);
            StringAssert.Contains(characterName, actualJoinBoardResponse.errors[0]);
            StringAssert.Contains("already exists on the board", actualJoinBoardResponse.errors[0]);
        }

        //todo move this to an input factory
        private IJoinBoardInputOutput CreateJoinBoardAction(IBoard board)
        {
            return new JoinBoardAction(board);
        }
    }
}
