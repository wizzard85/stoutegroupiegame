﻿using System.Linq;
using LMF.Game.Actions;
using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;
using NSubstitute;
using NUnit.Framework;

namespace LMF.Game.Tests.Actions
{
    [TestFixture]
    public class StartBattleActionTests
    {
        [Test]
        public void Start_GivenCharacterIsOnBoard_ShouldSendFinishBattle()
        {
            //Arrange
            var positionKey = "tile-1-2";
            var characterName = "Bob";
            var board = new BoardTestDataBuilder()
                .WithCharacters(characterName)
                .Build();

            var battleOutput = Substitute.For<IBattleOutput>();
            BattleRequest actualBattleRequest = null;
            battleOutput.FinishBattle(Arg.Do<BattleRequest>(request => actualBattleRequest = request));

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            Assert.NotNull(actualBattleRequest);
            Assert.AreEqual(positionKey, actualBattleRequest.positionKey);
            Assert.AreEqual(characterName, actualBattleRequest.characterName);
            Assert.Null(actualBattleRequest.errors);
        }

        [Test]
        public void Start_GivenCharacterIsOnBoard_ShouldNotSendStopBattle()
        {
            //Arrange
            var positionKey = "tile-15-62";
            var characterName = "Ver";
            var board = new BoardTestDataBuilder()
                .WithCharacters(characterName)
                .Build();

            var battleOutput = Substitute.For<IBattleOutput>();

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            battleOutput.DidNotReceive().StopBattle(Arg.Any<BattleRequest>());
        }

        [Test]
        public void Start_GivenCharacterIsNotOnBoard_ShouldSendStopBattle()
        {
            //Arrange
            var positionKey = "tile-45-34";
            var characterName = "Tubbeh";
            var board = new BoardTestDataBuilder()
                .Build();

            var battleOutput = Substitute.For<IBattleOutput>();
            BattleRequest actualBattleRequest = null;
            battleOutput.StopBattle(Arg.Do<BattleRequest>(request => actualBattleRequest = request));

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            Assert.NotNull(actualBattleRequest);
            Assert.NotNull(actualBattleRequest.errors);
            Assert.AreEqual(1, actualBattleRequest.errors.Count);
            StringAssert.Contains(characterName, actualBattleRequest.errors[0]);
            StringAssert.Contains("isn't on the board", actualBattleRequest.errors[0]);
        }

        [Test]
        public void Start_GivenCharacterIsNotOnBoard_ShouldNotSendFinishBattle()
        {
            //Arrange
            var positionKey = "tile-35-34";
            var characterName = "Tubbeh";
            var board = new BoardTestDataBuilder()
                .Build();

            var battleOutput = Substitute.For<IBattleOutput>();
            
            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            battleOutput.DidNotReceive().FinishBattle(Arg.Any<BattleRequest>());
        }

        [Test]
        public void Start_GivenPositionNotAssigned_ShouldAssignPositionToCharacter()
        {
            //Arrange
            var positionKey = "tile-46-12";
            var characterName = "Tubbeh";
            var board = new BoardTestDataBuilder()
                .WithCharacters(characterName)
                .Build();

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            var ownedPositions = board.Positions.Where(position => !string.IsNullOrEmpty(position.owner)).ToList();
            Assert.AreEqual(1, ownedPositions.Count);
            Assert.AreEqual(positionKey, ownedPositions[0].key);
            Assert.AreEqual(characterName, ownedPositions[0].owner);
        }

        [Test]
        public void Start_GivenPositionAssignedToAnotherCharacter_ShouldReassignPositionToCharacter()
        {
            //Arrange
            var positionKey = "tile-25-34";
            var characterName1 = "Tubbeh";
            var characterName2 = "Ayjax";
            var board = new BoardTestDataBuilder()
                .WithCharacters(characterName1, characterName2)
                .Build();

            var battleRequest1 = new BattleRequestBuilder()
                .WithCharacterName(characterName1)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction1 = CreateStartBattleAction(board);
            startBattleAction1.Start(battleRequest1);

            var battleRequest2 = new BattleRequestBuilder()
                .WithCharacterName(characterName2)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction2 = CreateStartBattleAction(board);
            //Act
            startBattleAction2.Start(battleRequest2);
            //Assert
            var ownedPositions = board.Positions.Where(position => !string.IsNullOrEmpty(position.owner)).ToList();
            Assert.AreEqual(1, ownedPositions.Count);
            Assert.AreEqual(positionKey, ownedPositions[0].key);
            Assert.AreEqual(characterName2, ownedPositions[0].owner);
        }

        [Test]
        public void Start_GivenOtherPositionsAssignedToCharacter_ShouldAssignPositionToCharacter()
        {
            //Arrange
            var positionKey1 = "tile-25-34";
            var positionKey2 = "tile-12-14";
            var characterName1 = "Tubbeh";
            var characterName2 = "Ayjax";
            var board = new BoardTestDataBuilder()
                .WithCharacters(characterName1, characterName2)
                .Build();

            var battleRequest1 = new BattleRequestBuilder()
                .WithCharacterName(characterName1)
                .WithPositionKey(positionKey1)
                .Build();
            var startBattleAction1 = CreateStartBattleAction(board);
            startBattleAction1.Start(battleRequest1);

            var battleRequest2 = new BattleRequestBuilder()
                .WithCharacterName(characterName2)
                .WithPositionKey(positionKey2)
                .Build();
            var startBattleAction2 = CreateStartBattleAction(board);
            //Act
            startBattleAction2.Start(battleRequest2);
            //Assert
            var ownedPositions = board.Positions.Where(position => !string.IsNullOrEmpty(position.owner)).ToList();
            Assert.AreEqual(2, ownedPositions.Count);
            Assert.AreEqual(positionKey2, ownedPositions[1].key);
            Assert.AreEqual(characterName2, ownedPositions[1].owner);
        }

        [Test]
        public void Start_GivenPositionIsNotOnBoard_ShouldSendStopBattle()
        {
            //Arrange
            var positionKey = "tile-1001-1001";
            var characterName = "Tubbeh";
            var board = new BoardTestDataBuilder()
                .WithCharacters(characterName)
                .Build();

            var battleOutput = Substitute.For<IBattleOutput>();
            BattleRequest actualBattleRequest = null;
            battleOutput.StopBattle(Arg.Do<BattleRequest>(request => actualBattleRequest = request));

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            Assert.NotNull(actualBattleRequest);
            Assert.NotNull(actualBattleRequest.errors);
            Assert.AreEqual(1, actualBattleRequest.errors.Count);
            StringAssert.Contains("Position doesn't exist on the board", actualBattleRequest.errors[0]);
        }

        [Test]
        public void Start_GivenPositionIsNotOnBoard_ShouldNotSendFinishBattle()
        {
            //Arrange
            var positionKey = "tile-999-999";
            var characterName = "Tubbeh";
            var board = new BoardTestDataBuilder()
                .WithCharacters(characterName)
                .Build();

            var battleOutput = Substitute.For<IBattleOutput>();

            var battleRequest = new BattleRequestBuilder()
                .WithCharacterName(characterName)
                .WithPositionKey(positionKey)
                .Build();
            var startBattleAction = CreateStartBattleAction(board, battleOutput);
            //Act
            startBattleAction.Start(battleRequest);
            //Assert
            battleOutput.DidNotReceive().FinishBattle(Arg.Any<BattleRequest>());
        }

        //todo move this to an input factory
        private IStartBattleInput CreateStartBattleAction(IBoard board, IBattleOutput battleOutput = null)
        {
            battleOutput = battleOutput ?? Substitute.For<IBattleOutput>();
            return new StartBattleAction(board, battleOutput);
        }
    }
}