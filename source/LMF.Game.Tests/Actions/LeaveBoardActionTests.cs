﻿using LMF.Game.Actions;
using LMF.Game.IO;
using LMF.Game.Models.Builders;
using NUnit.Framework;

namespace LMF.Game.Tests.Actions
{
    [TestFixture]
    public class LeaveBoardActionTests
    {
        [Test]
        public void Leave_GivenCharacterIsOnBoard_ShouldRemoveCharacterFromBoard()
        {
            //Arrange
            var characterName = "Mrs. Lance";
            var leavingCharacterName = "Tubbeh";
            var expectedCharacters = new[] { characterName };
            var leaveBoardRequest = new LeaveBoardRequestBuilder()
                .WithCharacterName(leavingCharacterName)
                .Build();
            var board = new BoardTestDataBuilder()
                .Build();
            board.AddCharacter(characterName);
            board.AddCharacter(leavingCharacterName);
            var leaveBoardAction = CreateLeaveBoardAction(board);
            //Act
            leaveBoardAction.Leave(leaveBoardRequest);
            //Assert
            CollectionAssert.AreEquivalent(expectedCharacters, board.Characters);
        }

        [Test]
        public void Leave_GivenCharacterIsNotOnBoard_ShouldLeaveCharactersUnchanged()
        {
            //Arrange
            var characterName = "Mrs. Lance";
            var leavingCharacterName = "Bob";
            var expectedCharacters = new[] { characterName };
            var leaveBoardRequest = new LeaveBoardRequestBuilder()
                .WithCharacterName(leavingCharacterName)
                .Build();
            var board = new BoardTestDataBuilder()
                .Build();
            board.AddCharacter(characterName);
            var leaveBoardAction = CreateLeaveBoardAction(board);
            //Act
            leaveBoardAction.Leave(leaveBoardRequest);
            //Assert
            CollectionAssert.AreEquivalent(expectedCharacters, board.Characters);
        }

        //todo move this to an input factory
        private ILeaveBoardInput CreateLeaveBoardAction(IBoard board)
        {
            return new LeaveBoardAction(board);
        }
    }
}
