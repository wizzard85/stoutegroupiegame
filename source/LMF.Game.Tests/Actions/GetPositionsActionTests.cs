﻿using LMF.Game.Actions;
using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;
using NUnit.Framework;

namespace LMF.Game.Tests.Actions
{
    [TestFixture]
    public class GetPositionsActionTests
    {
        [Test]
        public void Get_Given2By2TopLeftSectorOf6By6UniformHexGrid_ShouldReturnTopLeft4Positions()
        {
            //Arrange
            var sectorKey = "0-0";
            var expectedNumberOfPositions = 4;
            var expectedPosition1 = new PositionBuilder().WithKey("tile-0-1").WithSector(sectorKey).Build();
            var expectedPosition2 = new PositionBuilder().WithKey("tile-1-0").WithSector(sectorKey).Build();
            var expectedPosition3 = new PositionBuilder().WithKey("tile-0-0").WithSector(sectorKey).Build();
            var expectedPosition4 = new PositionBuilder().WithKey("tile-1-1").WithSector(sectorKey).Build();

            var boardDefinition = new UniformHexagonBoardDefinitionBuilder()
                .WithSize(6, 6)
                .WithSectorSize(2, 2)
                .Build();
            var board = new BoardTestDataBuilder()
                .WithDefinition(boardDefinition)
                .Build();

            var positionsRequest = new GetPositionsRequestBuilder()
                .WithSectorKey(sectorKey)
                .Build();
            var getPositionsAction = CreatePositionsAction(board);
            //Act
            var actualGetPositionsResponse = getPositionsAction.Get(positionsRequest);
            //Assert
            Assert.NotNull(actualGetPositionsResponse);
            Assert.AreEqual(expectedNumberOfPositions, actualGetPositionsResponse.positions.Count);
            Assert.Null(actualGetPositionsResponse.errors);
            AssertPositionsEqual(expectedPosition1, actualGetPositionsResponse.positions[0]);
            AssertPositionsEqual(expectedPosition2, actualGetPositionsResponse.positions[1]);
            AssertPositionsEqual(expectedPosition3, actualGetPositionsResponse.positions[2]);
            AssertPositionsEqual(expectedPosition4, actualGetPositionsResponse.positions[3]);
        }

        [Test]
        public void Get_Given2By2TopRightSector6By6UniformHexGrid_ShouldReturnTopRight2Positions()
        {
            //Arrange
            var sectorKey = "1-0";
            var expectedNumberOfPositions = 2;
            var expectedPosition1 = new PositionBuilder().WithKey("tile-2-0").WithSector(sectorKey).Build();
            var expectedPosition2 = new PositionBuilder().WithKey("tile-2-1").WithSector(sectorKey).Build();

            var boardDefinition = new UniformHexagonBoardDefinitionBuilder()
                .WithSize(6, 6)
                .WithSectorSize(2, 2)
                .Build();
            var board = new BoardTestDataBuilder()
                .WithDefinition(boardDefinition)
                .Build();

            var positionsRequest = new GetPositionsRequestBuilder()
                .WithSectorKey(sectorKey)
                .Build();
            var getPositionsAction = CreatePositionsAction(board);
            //Act
            var actualGetPositionsResponse = getPositionsAction.Get(positionsRequest);
            //Assert
            Assert.NotNull(actualGetPositionsResponse);
            Assert.AreEqual(expectedNumberOfPositions, actualGetPositionsResponse.positions.Count);
            Assert.Null(actualGetPositionsResponse.errors);
            AssertPositionsEqual(expectedPosition1, actualGetPositionsResponse.positions[0]);
            AssertPositionsEqual(expectedPosition2, actualGetPositionsResponse.positions[1]);
        }

        [Test]
        public void Get_Given2By2BottomLeftSectorOf6By6UniformHexGrid_ShouldReturnBottomLeft4Positions()
        {
            //Arrange
            var sectorKey = "0-2";
            var expectedNumberOfPositions = 4;
            var expectedPosition1 = new PositionBuilder().WithKey("tile-0-5").WithSector(sectorKey).Build();
            var expectedPosition2 = new PositionBuilder().WithKey("tile-1-4").WithSector(sectorKey).Build();
            var expectedPosition3 = new PositionBuilder().WithKey("tile-0-4").WithSector(sectorKey).Build();
            var expectedPosition4 = new PositionBuilder().WithKey("tile-1-5").WithSector(sectorKey).Build();

            var boardDefinition = new UniformHexagonBoardDefinitionBuilder()
                .WithSize(6, 6)
                .WithSectorSize(2, 2)
                .Build();
            var board = new BoardTestDataBuilder()
                .WithDefinition(boardDefinition)
                .Build();

            var positionsRequest = new GetPositionsRequestBuilder()
                .WithSectorKey(sectorKey)
                .Build();
            var getPositionsAction = CreatePositionsAction(board);
            //Act
            var actualGetPositionsResponse = getPositionsAction.Get(positionsRequest);
            //Assert
            Assert.NotNull(actualGetPositionsResponse);
            Assert.AreEqual(expectedNumberOfPositions, actualGetPositionsResponse.positions.Count);
            Assert.Null(actualGetPositionsResponse.errors);
            AssertPositionsEqual(expectedPosition1, actualGetPositionsResponse.positions[0]);
            AssertPositionsEqual(expectedPosition2, actualGetPositionsResponse.positions[1]);
            AssertPositionsEqual(expectedPosition3, actualGetPositionsResponse.positions[2]);
            AssertPositionsEqual(expectedPosition4, actualGetPositionsResponse.positions[3]);
        }

        [Test]
        public void Get_Given2By2BottomRightSector6By6UniformHexGrid_ShouldReturnBottomRight2Positions()
        {
            //Arrange
            var sectorKey = "1-2";
            var expectedNumberOfPositions = 2;
            var expectedPosition1 = new PositionBuilder().WithKey("tile-2-4").WithSector(sectorKey).Build();
            var expectedPosition2 = new PositionBuilder().WithKey("tile-2-5").WithSector(sectorKey).Build();

            var boardDefinition = new UniformHexagonBoardDefinitionBuilder()
                .WithSize(6, 6)
                .WithSectorSize(2, 2)
                .Build();
            var board = new BoardTestDataBuilder()
                .WithDefinition(boardDefinition)
                .Build();

            var positionsRequest = new GetPositionsRequestBuilder()
                .WithSectorKey(sectorKey)
                .Build();
            var getPositionsAction = CreatePositionsAction(board);
            //Act
            var actualGetPositionsResponse = getPositionsAction.Get(positionsRequest);
            //Assert
            Assert.NotNull(actualGetPositionsResponse);
            Assert.AreEqual(expectedNumberOfPositions, actualGetPositionsResponse.positions.Count);
            Assert.Null(actualGetPositionsResponse.errors);
            AssertPositionsEqual(expectedPosition1, actualGetPositionsResponse.positions[0]);
            AssertPositionsEqual(expectedPosition2, actualGetPositionsResponse.positions[1]);
        }

        [Test]
        public void Get_Given2By2CentreLeftSectorOf6By6UniformHexGrid_ShouldReturnCentreLeft4Positions()
        {
            //Arrange
            var sectorKey = "0-1";
            var expectedNumberOfPositions = 4;
            var expectedPosition1 = new PositionBuilder().WithKey("tile-0-3").WithSector(sectorKey).Build();
            var expectedPosition2 = new PositionBuilder().WithKey("tile-1-2").WithSector(sectorKey).Build();
            var expectedPosition3 = new PositionBuilder().WithKey("tile-0-2").WithSector(sectorKey).Build();
            var expectedPosition4 = new PositionBuilder().WithKey("tile-1-3").WithSector(sectorKey).Build();

            var boardDefinition = new UniformHexagonBoardDefinitionBuilder()
                .WithSize(6, 6)
                .WithSectorSize(2, 2)
                .Build();
            var board = new BoardTestDataBuilder()
                .WithDefinition(boardDefinition)
                .Build();

            var positionsRequest = new GetPositionsRequestBuilder()
                .WithSectorKey(sectorKey)
                .Build();
            var getPositionsAction = CreatePositionsAction(board);
            //Act
            var actualGetPositionsResponse = getPositionsAction.Get(positionsRequest);
            //Assert
            Assert.NotNull(actualGetPositionsResponse);
            Assert.AreEqual(expectedNumberOfPositions, actualGetPositionsResponse.positions.Count);
            Assert.Null(actualGetPositionsResponse.errors);
            AssertPositionsEqual(expectedPosition1, actualGetPositionsResponse.positions[0]);
            AssertPositionsEqual(expectedPosition2, actualGetPositionsResponse.positions[1]);
            AssertPositionsEqual(expectedPosition3, actualGetPositionsResponse.positions[2]);
            AssertPositionsEqual(expectedPosition4, actualGetPositionsResponse.positions[3]);
        }

        [Test]
        public void Get_Given2By2CentreRightSectorOf6By6UniformHexGrid_ShouldReturnCentreRight2Positions()
        {
            //Arrange
            var sectorKey = "1-1";
            var expectedNumberOfPositions = 2;
            var expectedPosition1 = new PositionBuilder().WithKey("tile-2-3").WithSector(sectorKey).Build();
            var expectedPosition2 = new PositionBuilder().WithKey("tile-2-2").WithSector(sectorKey).Build();

            var boardDefinition = new UniformHexagonBoardDefinitionBuilder()
                .WithSize(6, 6)
                .WithSectorSize(2, 2)
                .Build();
            var board = new BoardTestDataBuilder()
                .WithDefinition(boardDefinition)
                .Build();

            var positionsRequest = new GetPositionsRequestBuilder()
                .WithSectorKey(sectorKey)
                .Build();
            var getPositionsAction = CreatePositionsAction(board);
            //Act
            var actualGetPositionsResponse = getPositionsAction.Get(positionsRequest);
            //Assert
            Assert.NotNull(actualGetPositionsResponse);
            Assert.AreEqual(expectedNumberOfPositions, actualGetPositionsResponse.positions.Count);
            Assert.Null(actualGetPositionsResponse.errors);
            AssertPositionsEqual(expectedPosition1, actualGetPositionsResponse.positions[0]);
            AssertPositionsEqual(expectedPosition2, actualGetPositionsResponse.positions[1]);
        }

        private void AssertPositionsEqual(Position expectedPosition, Position actualPosition)
        {
            Assert.AreEqual(expectedPosition.key, actualPosition.key);
            Assert.AreEqual(expectedPosition.owner, actualPosition.owner);
            Assert.AreEqual(expectedPosition.sector, actualPosition.sector);
        }

        private IGetPositionsInputOutput CreatePositionsAction(Board board)
        {
            return new GetPositionsAction(board);
        }
    }
}