using System.Collections.Generic;
using LMF.Game.Models;

namespace LMF.Game
{
    public interface IBoardDefinition
    {
        IEnumerable<Position> CreatePositions();
    }
}