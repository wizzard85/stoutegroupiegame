using System;
using System.Collections.Generic;
using LMF.Game.Models;
using LMF.Game.Models.Builders;

namespace LMF.Game
{
    public class UniformHexagonBoardDefinition : IBoardDefinition
    {
        public int sectorSizeX { get; }
        public int sectorSizeY { get; }
        public int sizeX { get; set; }
        public int sizeY { get; set; }

        public UniformHexagonBoardDefinition(int sizeX, int sizeY, int sectorSizeX, int sectorSizeY)
        {
            this.sectorSizeX = sectorSizeX;
            this.sectorSizeY = sectorSizeY;
            this.sizeX = sizeX;
            this.sizeY = sizeY;
        }

        //todo try to figure out how this can be used on the client side with out duplicating it
        public IEnumerable<Position> CreatePositions()
        {
            for (int positionX = 0; positionX < sizeX / 2; positionX++)
            {
                for (int positionY = 0; positionY < sizeY; positionY++)
                {
                    var sectorX = ((int)Math.Floor((decimal)positionX/sectorSizeX));
                    var sectorY = ((int)Math.Floor((decimal)positionY/sectorSizeY));
                    var positionKey = string.Format("{0}-{1}-{2}", "tile", positionX, positionY);
                    var sectorKey = string.Format("{0}-{1}", sectorX, sectorY);

                    yield return new PositionBuilder()
                        .WithKey(positionKey)
                        .WithSector(sectorKey)
                        .Build();
                }
            }
        }
    }
}