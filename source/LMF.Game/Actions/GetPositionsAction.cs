using System.Linq;
using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;

namespace LMF.Game.Actions
{
    //todo rename this to GetSectorBoardPositionsAction
    public class GetPositionsAction : IGetPositionsInputOutput
    {
        private IBoard _board;

        public GetPositionsAction(IBoard board)
        {
            _board = board;
        }

        public GetPositionsResponse Get(GetPositionsRequest request)
        {
            var sectorPositions = _board.Positions.Where(position => position.sector == request.sectorKey).ToList();
            var getPositionsResponse = new GetPositionsResponseBuilder()
                .WithPositions(sectorPositions)
                .Build();
            return getPositionsResponse;
        }
    }
}