﻿using LMF.Game.IO;
using LMF.Game.Models;
using LMF.Game.Models.Builders;

namespace LMF.Game.Actions
{
    public class JoinBoardAction : IJoinBoardInputOutput
    {
        private readonly IBoard _board;

        public JoinBoardAction(IBoard board)
        {
            _board = board;
        }

        public JoinBoardResponse Join(JoinBoardRequest joinBoardRequest)
        {
            if (_board.AddCharacter(joinBoardRequest.characterName))
            {
                return new JoinBoardResponseBuilder()
                    .WithBoardDefinition(_board.Definition)
                    .Build();
            }

            return new JoinBoardResponseBuilder()
                .WithErrors(string.Format("The character '{0}' already exists on the board.", joinBoardRequest. characterName))
                .Build();
        }
    }
}