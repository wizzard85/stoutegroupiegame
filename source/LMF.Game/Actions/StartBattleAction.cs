using System.Collections.Generic;
using System.Linq;
using LMF.Game.IO;
using LMF.Game.Models;

namespace LMF.Game.Actions
{
    public class StartBattleAction : IStartBattleInput
    {
        private readonly IBoard _board;
        private readonly IBattleOutput _battleOutput;

        public StartBattleAction(IBoard board, IBattleOutput battleOutput)
        {
            _board = board;
            _battleOutput = battleOutput;
        }

        public void Start(BattleRequest battleRequest)
        {
            if (!_board.ContainsCharacter(battleRequest.characterName))
            {
                AddCharacterErrorToBattle(battleRequest);
                _battleOutput.StopBattle(battleRequest);
                return;
            }

            if (!_board.ContainsPosition(battleRequest.positionKey))
            {
                AddPositionErrorToBattle(battleRequest);
                _battleOutput.StopBattle(battleRequest);
                return;
            }

            _board.AssignPosition(battleRequest.positionKey, battleRequest.characterName);
            _battleOutput.FinishBattle(battleRequest);
        }
        
        //todo it feels like this method should be in a builder, so that you create a new battle request from the existing one and call .WithErrors() or something
        private void AddPositionErrorToBattle(BattleRequest battleRequest)
        {
            AddErrorToBattle(battleRequest, "Position doesn't exist on the board.");
        }

        //todo it feels like this method should be in a builder, so that you create a new battle request from the existing one and call .WithErrors() or something
        private void AddCharacterErrorToBattle(BattleRequest battleRequest)
        {
            AddErrorToBattle(battleRequest, string.Format("Character '{0}' isn't on the board.", battleRequest.characterName));
        }

        private static void AddErrorToBattle(BattleRequest battleRequest, string errorMessage)
        {
            battleRequest.errors = battleRequest.errors ?? new List<string>();
            battleRequest.errors.Add(errorMessage);
        }
    }
}