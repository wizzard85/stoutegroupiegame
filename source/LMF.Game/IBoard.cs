﻿using System.Collections.Generic;
using LMF.Game.Models;

namespace LMF.Game
{
    public interface IBoard
    {
        bool AddCharacter(string characterName);
        bool RemoveCharacter(string characterName);
        void AssignPosition(string positionKey, string characterName);

        bool ContainsCharacter(string characterName);
        bool ContainsPosition(string positionKey);

        string Name { get; set; }
        IBoardDefinition Definition { get; set; }
        IEnumerable<string> Characters { get; }
        IEnumerable<Position> Positions { get; }
    }
}