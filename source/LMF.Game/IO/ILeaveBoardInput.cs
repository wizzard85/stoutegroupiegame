using LMF.Game.Models;

namespace LMF.Game.IO
{
    public interface ILeaveBoardInput
    {
        void Leave(LeaveBoardRequest leaveBoardRequest);
    }
}