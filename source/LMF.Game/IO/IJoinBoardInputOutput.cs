﻿using LMF.Game.Models;

namespace LMF.Game.IO
{
    public interface IJoinBoardInputOutput
    {
        JoinBoardResponse Join(JoinBoardRequest joinBoardRequest);
    }
}