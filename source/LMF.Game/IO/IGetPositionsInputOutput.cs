using LMF.Game.Models;

namespace LMF.Game.IO
{
    public interface IGetPositionsInputOutput
    {
        GetPositionsResponse Get(GetPositionsRequest request);
    }
}