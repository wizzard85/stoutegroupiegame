﻿using LMF.Game.Actions;
using LMF.Game.IO;

namespace LMF.Game
{
    public class InputFactory
    {
        private readonly IBoard _board;
        private readonly IBattleOutput _battleOutput;

        public InputFactory(IBoard board, IBattleOutput battleOutput)
        {
            _board = board;
            _battleOutput = battleOutput;
        }

        public ILeaveBoardInput CreateLeaveBoardInput()
        {
            return new LeaveBoardAction(_board);
        }

        public IJoinBoardInputOutput CreateJoinBoardInput()
        {
            return new JoinBoardAction(_board);
        }

        public IStartBattleInput CreateBattleInput()
        {
            return new StartBattleAction(_board, _battleOutput);
        }
    }
}
