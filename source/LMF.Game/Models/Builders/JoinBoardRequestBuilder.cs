﻿namespace LMF.Game.Models.Builders
{
    public class JoinBoardRequestBuilder
    {
        private string _characterName;

        public JoinBoardRequestBuilder WithCharacterName(string characterName)
        {
            _characterName = characterName;
            return this;
        }

        public JoinBoardRequest Build()
        {
            return new JoinBoardRequest()
            {
                characterName = _characterName
            };
        }
    }
}