namespace LMF.Game.Models.Builders
{
    public class GetPositionsRequestBuilder
    {
        private string _sectorKey;

        public GetPositionsRequestBuilder WithSectorKey(string sectorKey)
        {
            _sectorKey = sectorKey;
            return this;
        }

        public GetPositionsRequest Build()
        {
            return new GetPositionsRequest() {
                sectorKey = _sectorKey
            };
        }
    }
}