namespace LMF.Game.Models.Builders
{
    public class UniformHexagonBoardDefinitionBuilder
    {
        private int _sizeX;
        private int _sizeY;
        private int _sectorSizeX = 1;
        private int _sectorSizeY = 1;

        public UniformHexagonBoardDefinitionBuilder WithSizeX(int sizeX)
        {
            _sizeX = sizeX;
            return this;
        }

        public UniformHexagonBoardDefinitionBuilder WithSizeY(int sizeY)
        {
            _sizeY = sizeY;
            return this;
        }

        public UniformHexagonBoardDefinitionBuilder WithSize(int sizeX, int sizeY)
        {
            _sizeX = sizeX;
            _sizeY = sizeY;
            return this;
        }

        public UniformHexagonBoardDefinitionBuilder WithSectorSize(int sizeX, int sizeY)
        {
            _sectorSizeY = sizeY;
            _sectorSizeX = sizeX;
            return this;
        }

        public UniformHexagonBoardDefinition Build()
        {
            return new UniformHexagonBoardDefinition(_sizeX, _sizeY, _sectorSizeX, _sectorSizeY);
        }
    }
}