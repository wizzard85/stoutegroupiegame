﻿using System.Collections.Generic;

namespace LMF.Game.Models.Builders
{
    public class GetPositionsResponseBuilder
    {
        private IEnumerable<Position> _sectorPositions;

        public GetPositionsResponseBuilder WithPositions(IEnumerable<Position> sectorPositions)
        {
            _sectorPositions = sectorPositions;
            return this;
        }

        public GetPositionsResponse Build()
        {
            return new GetPositionsResponse()
            {
                positions = new List<Position>(_sectorPositions)
            };
        }
    }
}