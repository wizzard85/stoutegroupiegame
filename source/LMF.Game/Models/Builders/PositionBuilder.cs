namespace LMF.Game.Models.Builders
{
    public class PositionBuilder
    {
        private string _key;
        private string _sector;
        private string _owner;

        public PositionBuilder WithKey(string key)
        {
            _key = key;
            return this;
        }

        public PositionBuilder WithSector(string sector)
        {
            _sector = sector;
            return this;
        }

        public PositionBuilder WithOwner(string owner)
        {
            _owner = owner;
            return this;
        }

        public Position Build()
        {
            return new Position(_key, _sector)
            {
                owner = _owner
            };
        }
    }
}