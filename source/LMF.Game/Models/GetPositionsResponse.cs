using System.Collections.Generic;

namespace LMF.Game.Models
{
    public class GetPositionsResponse
    {
        public bool hasErrors
        {
            get { return errors != null && errors.Count > 0; }
        }
        //todo model a proper error object
        public List<Position> positions { get; set; }
        public List<string> errors { get; set; }
    }
}