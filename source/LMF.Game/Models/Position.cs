namespace LMF.Game.Models
{
    public class Position
    {
        public Position(string key, string sector)
        {
            this.key = key;
            this.sector = sector;
        }

        public string key { get; private set; }
        public string sector { get; private set; }
        public string owner { get; set; }
    }
}