using System.Collections.Concurrent;
using System.Collections.Generic;
using LMF.Game.Models;

namespace LMF.Game
{
    //todo it feels like Board needs a container, something like a world, but for now we'll just use the board as is because we aren't supporting multiple boards yet
    public class Board : IBoard
    {
        private readonly ConcurrentDictionary<string, byte> _characters = new ConcurrentDictionary<string, byte>();
        private readonly ConcurrentDictionary<string, Position> _positions = new ConcurrentDictionary<string, Position>();        

        public Board(IBoardDefinition boardDefinition)
        {
            Name = "Board";
            Definition = boardDefinition;
            PopulatePositions();
        }

        public bool AddCharacter(string characterName)
        {
            if (_characters.ContainsKey(characterName)) return false;
            _characters.AddOrUpdate(characterName, 0, (s, b) => 0);
            return true;
        }

        public bool RemoveCharacter(string characterName)
        {
            byte value;
            return _characters.TryRemove(characterName, out value);
        }

        public void AssignPosition(string positionKey, string characterName)
        {
            //todo this isn't thread safe, write a test that will force me to make it thread safe
            _positions[positionKey].owner = characterName;
        }

        public bool ContainsPosition(string positionKey)
        {
            return _positions.ContainsKey(positionKey);
        }

        public bool ContainsCharacter(string characterName)
        {
            return _characters.ContainsKey(characterName);
        }

        private void PopulatePositions()
        {
            foreach (var position in Definition.CreatePositions())
            {
                _positions[position.key] = position;
            }
        }

        public string Name { get; set; }
        public IBoardDefinition Definition { get; set; }

        public IEnumerable<string> Characters
        {
            get { return _characters.Keys; }
        }

        public IEnumerable<Position> Positions
        {
            get { return _positions.Values; }
        }
    }
}